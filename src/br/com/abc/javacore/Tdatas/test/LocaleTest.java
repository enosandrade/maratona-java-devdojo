package br.com.abc.javacore.Tdatas.test;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by William Suane on 7/6/2016.
 */
public class LocaleTest {
    public static void main(String[] args) {
        //ISO 639 pt= portugues BR - Brazil, en - ingles, US - Estados Unidos
        Locale locItalia = new Locale("it");
        Locale locSuica = new Locale("it", "CH");
        Locale locBrasil = new Locale("pt", "BR");
        Locale locUSA = new Locale("en", "US");

        Calendar c = Calendar.getInstance();
        c.set(2015, Calendar.DECEMBER, 23);
        DateFormat df = DateFormat.getDateInstance(DateFormat.FULL, locItalia);
        DateFormat df2 = DateFormat.getDateInstance(DateFormat.FULL, locSuica);
        DateFormat df3 = DateFormat.getDateInstance(DateFormat.FULL, locBrasil);
        DateFormat df4 = DateFormat.getDateInstance(DateFormat.FULL, locUSA);

        System.out.println(df.format(c.getTime()));
        System.out.println(df2.format(c.getTime()));
        System.out.println(df3.format(c.getTime()));
        System.out.println(df4.format(c.getTime()));
        printHelloWorld(locBrasil);
        printHelloWorld(locUSA);

        System.out.println(locBrasil.getDisplayCountry());
        System.out.println(locBrasil.getDisplayLanguage());
        System.out.println(locUSA.getDisplayCountry(locUSA));
        System.out.println(locUSA.getDisplayLanguage(locUSA));

    }

    private static void printHelloWorld(Locale locale) {
        Locale ptBR = new Locale("pt", "BR");
        Locale enUS = new Locale("en", "US");

        if (locale.getLanguage().equals("pt")) {
            System.out.println("Olá, Mundo!");
        } else if (locale.getLanguage().equals("en")) {
            System.out.println("Hello World");
        }


    }
}
