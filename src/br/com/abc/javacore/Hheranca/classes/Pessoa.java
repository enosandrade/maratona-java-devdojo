package br.com.abc.javacore.Hheranca.classes;

/**
 * Created by William Suane on 5/4/2016.
 */
public class Pessoa {
    protected String nome;
    protected String cpf;

    protected Endereco endereco;


    public Pessoa(String nome){
        System.out.println("Dentro do construtor de Pessoa");
        this.nome = nome;
    }

    {
        System.out.println("Bloco de inicialização 1 de Pessoa");
    }
    {
        System.out.println("Bloco de inicialização 2 de Pessoa");
    }
    static {
        System.out.println("Bloco de inicialização estatico de Pessoa");
    }

    public Pessoa(String nome, String cpf) {
        this(nome);
        this.cpf = cpf;
    }




    public  void imprimir(){
        System.out.println("Nome: " + this.nome);
        System.out.println("CPF: " + this.cpf);
        System.out.println("Rua: " + this.endereco.getRua());
        System.out.println("Bairro: " + this.endereco.getBairro());

    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }
}
