package br.com.abc.javacore.Hheranca.classes;

/**
 * Created by William Suane on 5/4/2016.
 */
public class Funcionario extends Pessoa {

    private double salario;

    public Funcionario(String nome) {
        super(nome);
        System.out.println("Dentro do construtor Funcionário");
    }
    {
        System.out.println("Bloco de inicialização 1 de Funcionario");
    }
    {
        System.out.println("Bloco de inicialização 2 de Funcionario");
    }
    static {
        System.out.println("Bloco de inicialização estatico de Funcionario");
    }

    public double getSalario() {
        return salario;
    }

    @Override
    public void imprimir(){
        super.imprimir();
        System.out.println("Salário: " + salario);
        imprimirReciboPagamento();
    }

    public void imprimirReciboPagamento(){
        System.out.println("Eu "+ super.nome+" recebi o pagamento de "+this.salario);
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }
}
