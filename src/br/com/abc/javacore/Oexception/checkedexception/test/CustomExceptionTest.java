package br.com.abc.javacore.Oexception.checkedexception.test;

import br.com.abc.javacore.Oexception.customexceptions.LoginInvalidoException;

/**
 * Created by William Suane on 6/22/2016.
 */
public class CustomExceptionTest {
    public static void main(String[] args) {
        try {
            logar();
        } catch (LoginInvalidoException e) {
            System.out.println(e.getMessage());
        }
    }
    private static void logar() throws  LoginInvalidoException{
        String usuario = "123";
        String senha = "111";
        String usuarioDigitado = "Goku";
        String senhaDigitada = "123";

        if(!usuario.equals(usuarioDigitado) || !senha.equals(senhaDigitada)){
            throw  new LoginInvalidoException();
        }else{
            System.out.println("Logado");
        }
    }

}
