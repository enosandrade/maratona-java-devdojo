package br.com.abc.javacore.Npolimorfismo.test;

import br.com.abc.javacore.Npolimorfismo.classes.Funcionario;
import br.com.abc.javacore.Npolimorfismo.classes.Gerente;
import br.com.abc.javacore.Npolimorfismo.classes.RelatorioPagamento;
import br.com.abc.javacore.Npolimorfismo.classes.Vendedor;

/**
 * Created by William Suane on 6/1/2016.
 */
public class PolimorfismoTest {
    public static void main(String[] args) {

        Gerente g = new Gerente("Enos",5000,4000);
        Vendedor p = new Vendedor("Osvaldo", 2000,30000);

        RelatorioPagamento relatorios = new RelatorioPagamento();

        relatorios.relatorioPagamentoGenerico(g);
        relatorios.relatorioPagamentoGenerico(p);

    }
}
