package br.com.abc.javacore.Npolimorfismo.classes;

/**
 * Created by William Suane on 6/1/2016.
 */
public class RelatorioPagamento {

    public void relatorioPagamentoGerente(Gerente gerente){
        System.out.println("Gerando relatório de pagamento para a gerência");
        gerente.calcularPagamento();
        System.out.println("Nome: "+ gerente.getNome());
        System.out.println("Salario desse mês: "+ gerente.getSalario());
    }

    public void relatorioPagamentoVendedor(Vendedor vendedor){
        System.out.println("Gerando relatório de pagamento para os vendedores");
        vendedor.calcularPagamento();
        System.out.println("Nome: "+ vendedor.getNome());
        System.out.println("Salario desse mês: "+ vendedor.getSalario());
    }
    public void relatorioPagamentoGenerico(Funcionario funcionario){
        System.out.println("==========================");
        if(funcionario instanceof Vendedor){
            System.out.println("Vendedor");
        }else if(funcionario instanceof Gerente){
            System.out.println("Gerente");
        }

        funcionario.calcularPagamento();
        System.out.println("Nome: "+ funcionario.getNome());
        System.out.println("Salario desse mês: "+ funcionario.getSalario());
        if(funcionario instanceof Gerente){
            System.out.println("PNL: "+((Gerente) funcionario).getPnl());
        }
        if(funcionario instanceof Vendedor){
            System.out.println("Total das vendas: "+((Vendedor) funcionario).getTotalVendas());
        }
    }
}
