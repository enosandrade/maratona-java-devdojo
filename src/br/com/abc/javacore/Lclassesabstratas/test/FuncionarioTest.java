package br.com.abc.javacore.Lclassesabstratas.test;

import br.com.abc.javacore.Lclassesabstratas.classes.Funcionario;
import br.com.abc.javacore.Lclassesabstratas.classes.Gerente;
import br.com.abc.javacore.Lclassesabstratas.classes.Vendedor;

/**
 * Created by William Suane on 5/25/2016.
 */
public class FuncionarioTest {
    public static void main(String[] args) {

        Gerente g = new Gerente("Anna","11433-2",2000);
        Vendedor v = new Vendedor("Camilla", "22221-3",1500,5000);
        g.calcularSalario();
        v.calcularSalario();

        System.out.println(g);
        System.out.println("--------------");
        System.out.println(v);

    }
}
