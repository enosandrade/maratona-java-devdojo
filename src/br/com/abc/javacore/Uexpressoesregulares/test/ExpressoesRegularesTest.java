package br.com.abc.javacore.Uexpressoesregulares.test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by William Suane on 7/7/2016.
 */
public class ExpressoesRegularesTest {
  public static void main(String[] args) {
    // \d - todos os digitos
    // \D tudo o que não for digito
    // \s espaços em branco \t \n \f \r
    // \S caractere que não é branco
    // \w caracteres de palavras a-z A-Z, digitos e _
    // \W tudo o que não for caractere de palavra
    // []
    // ? zero ou uma
    // * zero ou mais
    // + uma ou mais
    // {n,m} de n ate m
    // ( )
    // |
    // $
    // . coringa 1.3 = 123,133,1A3,1#3, 1 3
    // ^ [^abc]


//        String texto = "12 0x 0xFFABC 0x100";
//        String regex = "0[xX][0-9A-F]+(\\s|$)";
//    String regex = "([\\w.-])+@([a-zA-Z])+(\\.([a-zA-Z])+)+";
//    String texto = "fulan-o@hotmail.com,102Abc_@gmail.com.br teste@mail";
      String regex = "([\\w.-])+@([a-zA-Z])+(\\.([a-zA-Z])+)+";
      String texto = "fulan-o@hotmail.com,102Abc_@gmail.com.br teste@mail";

      Pattern pattern = Pattern.compile(regex);
      Matcher matcher = pattern.matcher(texto);

      System.out.println("Texto: " + texto);
      System.out.println("Expressão: " + matcher.pattern());
      System.out.println("Posições encontradas: ");
      while (matcher.find()) {
        System.out.println(matcher.start() + " " + matcher.group());
      }
      System.out.println("enosads@gmail.com é um email valido? " + "enosads@gmail.com".matches(regex));
  }
}
